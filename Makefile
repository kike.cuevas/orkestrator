#
# Makefile for Kool Orkestrator
#
# author: jorge.medina@koolops.com.mx
# desc: Script to build the vagrant development environment.

NAME = orkestrator
VERSION = 1.0.7
PROJECT_NAME=local
PROJECT_ENV=development

.PHONY: all vagrant bootstrap build test deploy clean destroy help

all: help

vagrant:
	@echo ""
	@echo "Building ${PROJECT_NAME} ${PROJECT_ENV} environment based on vagrant."
	@echo ""
	vagrant up
	vagrant status
	vagrant ssh

bootstrap:
	@echo ""
	@echo "Bootstraping ${PROJECT_NAME} ${PROJECT_ENV} environment based on vagrant."
	@echo ""
	rm -rf /etc/ansible;
	ln -fs /vagrant /etc/ansible
	cd /etc/ansible
	bin/bootstrap.sh

build:
	@echo ""
	@echo "Building ${PROJECT_NAME} ${PROJECT_ENV} environment."
	@echo ""
	cd /etc/ansible
	bin/build.sh ${PROJECT_NAME} ${PROJECT_ENV}

test:
	@echo ""
	@echo "Testing ${PROJECT_NAME} ${PROJECT_ENV} environment."
	@echo ""
	cd /etc/ansible
	ansible-playbook central-site.yml --syntax-check

deploy:
	@echo ""
	@echo "Deploying central devops service."
	@echo ""
	cd /etc/ansible
	bin/deploy-central-devops-server.sh
	bin/postsetup.sh

clean:
	@echo ""
	@echo "Cleaning ${PROJECT_NAME} ${PROJECT_ENV} environment."
	@echo ""
	cd /etc/ansible
	git checkout -- ansible.cfg inventory/${PROJECT_NAME}/${PROJECT_ENV}
	rm -rf *.retry *.log

destroy:
	@echo ""
	@echo "Destroying ${PROJECT_NAME} ${PROJECT_ENV} environment."
	@echo ""
	vagrant destroy -f
	vagrant global-status --prune

help:
	@echo ""
	@echo "Please use 'make <target>' where <target> is one of:"
	@echo ""
	@echo "  vagrant   Provision the ${PROJECT_NAME} ${PROJECT_ENV} environment."
	@echo "  bootstrap Bootstrap the ${PROJECT_NAME} ${PROJECT_ENV} environment."
	@echo "  build     Builds the ${PROJECT_NAME} ${PROJECT_ENV} environment."
	@echo "  test      Tests for the ${PROJECT_NAME} ${PROJECT_ENV} environment."
	@echo "  deploy    Deploy the ${PROJECT_NAME} ${PROJECT_ENV} environment."
	@echo "  clean     Cleans ${PROJECT_NAME} changes on ansible settings."
	@echo "  destroy   Destroys the ${PROJECT_NAME} ${PROJECT_ENV} environment."
	@echo ""
	@echo ""
