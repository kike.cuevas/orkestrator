# Construyendo un Sistema de Orquestación de TI con Ansible y Jenkins

# Table of contents

1. [Introducción](#introducci-n)
2. [Arquitectura](#arquitectura)
3. [Requisitos](#requisitos)
4. [Preparación](Preparaci-on)
5. [Construcción](#construcci-n)
6. [Despliegue](#despliegue)
7. [Limpieza](#limpieza)
8. [Destrucción](#destrucci-on)
9. [Mejores prácticas](#mejorespracticas)
10. [Referencias](#referecias)

## Introducción

Esta es la guía que te permitirá construir un sistema Orquestación de TI con Ansible y Jenkins. El sistema de orquestación de TI esta diseñado para ayudar a optimizar las tareas de rutina y mantenimientos de los equipos de operaciones de TI, tanto en los procesos operativos, como en los de infraestructura.

### Objetivos

El objetivo de esta guía es simplificar el proceso de construcción de un sistema de orquestación de TI con Ansible y Jenkins.

- Definir los requisitos del flujo del sistema
- Introducir Git y Github Flow
- Introducir Ansible como motor de orquestación
- Introducir Jenkins como hub de automatización
- Como construir un ambiente de desarrollo local con vagrant
- Como personalizar la construcción del ambiente
- Como limpiar y destruir el ambiente vagrant

### Arquitectura

TODO.

## Requisitos

### El Flujo

Para optimizar los procesos de puesta a producción de nuevos desarrollos y servicios, involucraremos tanto a personas, sistemas y datos. El sistema de orquestación de TI depende de un flujo de trabajo colaborativo entre las diferentes funciones de las operaciones de TI, este flujo de trabajo esta basado en principios y prácticas del desarrollo de software ágil como eXtreme Programming, Agile, Continuous Delivery, Infrastructure as Code y DevOps.

Por lo tanto, nuestro flujo de trabajo está basado en un Pipeline de Continuous Delivery, se ve así:

SCM -> BUILD -> TEST -> DEPLOY

### SCM

Todos los scripts, archivos de configuración, plantillas, playbooks estarán almacenados en un repositorio de código basado en Git para mantenerlo versionado, para esto usaremos la plataforma Github con repositorios público, sin embargo, se pueden usar otras plataformas como Bitbucket para proyectos privados, o bien usar GitLab en infraestructura privada.

Para mantener un mejor control de cambios en el código fuente de la infraestructura, usaremos un flujo de trabajo basado en branches para las diferentes fases del desarrollo, pruebas y produción, por lo tanto nuestro flujo de trabajo estará basado en Github Flow.

### Ansible

Ansible es una herramienta diseñada para ayudar a las organizaciones en automatizar las actividades de administración de configuraciones, despliegue de aplicacinoes y orquestación de TI.

Con Ansible podremos orquestar la automatización de las siguientes tareas y reducir los tiempos de entrega al tener procedimientos repetibles y reproducibles:

* Aprovisionamiento de servidores físicos, máquinas virtuales e instancias en la nube
* Instalación y configuracon inicial de servidores
* Instalación y configuración de servicios de infraestructura
* Instalación y configuración de aplicaciones y bases de datos.
* Orquestación para realizar tareas complejas donde involucre más de un sistema, como pueden ser sistemas de respaldos, monitorización, balanceadores de carga, clusters, etc.

Ansible puede ser usado tanto por equipos de desarrollo para incluir en su código las instrucciones para deployar la aplicación y para operaciones de TI en orquestar los despliegues de ambientes, tanto físicos y virtuales.

### Jenkins

Con Jenkins automatizamos la ejecución de tareas como Build y Test del código de la infraestructura, y también la ejecución de los playbooks de Ansible para orquestar diferentes despliegues, con tareas tales como aprovisionamiento de ambientes de TI, gestión de configuraciones y despliegue de bases de datos, aplicativos y servicios de infraestrucutra y comunicaciones.

### Integración Ansible Jenkins

Se instaló el plugin Ansible en Jenkins para ejecutar los playbooks de Ansible desde la interfaz web de Jenkins, esto permite ejecutar de forma controlada los trabajos de ansible.

### Ambiente de desarrollo localhost

Se recomienda que los desarrolladores tengan la capacidad de construir un ambiente de desarrollo local que sea parecido los ambientes de pruebas y producción, por lo tanto se ha facilitado crear un ambiente basado en vagrant, de manera que desarrollador pueda construir el sistema desde su laptop o equipo de escritorio, se necesita lo siguiente:

 * Ubuntu 16.04 (Xenial) Linux Desktop
 * VirtualBox 5.2
 * Vagrant 2.1
 * git
 * make

## Preparación

Si ya instalaste virtualbox y vagrant, asegurate de instalar las siguientes dependencias para construir
el ambiente:

```shell
$ sudo apt-get install git make
```

Vayamos al directorio de trabajo:

```shell
$ cd ~/data/vcs/
$ git clone https://github.com/koolops/orkestrator.git
$ cd orkestrator
```

## Construcción

Para construir un ambiente de desarrollo local para el sistema de orquestación de TI usaremos el comando make y las configuraciones definidas en el archivo Makefile.

Ejecutaremos el subproceso vagrant para crear un ambiente de desarrollo local basado en Vagrant y Virtualbox, crearemos un par de máquinas virtuales basadas en la distribución GNU/Linux Ubuntu server 16.04 de 64-bits, una máquina se llama *devops* y será donde construiremos el sistema de orequestación y la otra se llama *cms* que es donde desplegaremos el servicio Wordpress.

Ejecuta el siguiente procedimiento dentro del directorio en el cual descargaste el repositorio de git, por ejemplo:

```shell
$ make vagrant
```

Una vez que las máquinas virtuales se hayan creado, continuamos el proceso de construcción del sistema de orquestación.

Lo primero que debes hacer es conectarte a la máquina devops, por ejemplo:

```shell
$ vagrant ssh devops
Welcome to Ubuntu 16.04.2 LTS (GNU/Linux 4.4.0-75-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

0 packages can be updated.
0 updates are security updates.


vagrant@devops:~$
```

Una vez que estés dentro de la máquina virtual devops, escala privilegios a la cuenta root y cambiate al directorio */vagrant*, por ejemplo:

```shell
vagrant@devops:~$ sudo -i

root@devops:~# cd /vagrant

```

Continuaremos usando make para ejecutar los diferentes subprocesos que nos permitirán consturir el sistema de orquestación de TI.

Primero debemos hacer el target *bootstrap*, el cual inicializa el sistema, es decir, instalaremos ansible y todos su requisitos para empezar a construir el sistema de orquestación.

```shell
root@devops:/vagrant# make bootstrap
```

Después de haber terminado el bootstrap en el sistema devops, ejecuta el target *build* para construir el ambiente de desarrollo local y las personalizaciones para ansible.

Antes debemos personalizar el ambiente, cambiamos:

```
...
...
...
[central:vars]
timezone=America/Mexico_City
# This user has unlimited sudo privileges, it is created at install time.
useradmin_name=vagrant

[central-devops]
devops.example.com  ansible_ssh_host=192.168.33.10
```


```shell
root@devops:/vagrant# make build
```

Si el subproceso de build para el entorno de desarrollo local termina exitosamente, ejecuta el target *test* para validar la sintaxis de los playbooks de Ansible.

```shell
root@devops:/vagrant# make test
```

## Despliegue

Después de haber construido el ambiente de desarrollo local para el sistema de orquestación, lo deployamos en la máquina virtual devops usando el parámetro *deploy*, por ejemplo:

```shell
root@devops:/vagrant# make deploy
```

Una vez que termine el proceso de deploy, ingresa a la consola de Jenkins desde el navegador en tu equipo local apuntando al siguiente URL: http://localhost:1088.

**IMPORTANTE:** El puerto *1088* está definido en la configuración de vagrant, este puerto se abre en el localhost y es re enviado al puerto *8080* de la maquina devops.

Las credenciales de acceso a Jenkins predefinidas son:

 - Usuario: admin
 - Contraseña: admin

Para conocer más acerca del uso de Jenkins sigue con la guía operación del sistema de orquestación de TI.

Para desplegar el servicio wordpress, se puede hacer directamente ejecutando el playbook de ansible, por ejemplo:

```shell
root@devops:/vagrant# cd /etc/ansible
root@devops:/etc/ansible# ansible-playbook central-cms-servers.yml
```
Una vez que termine el proceso de deploy wordpress, ingresa al asistente de configuración de wordpress desde el navegador en tu equipo local apuntando al siguiente URL: http://localhost:1080.

**IMPORTANTE:** El puerto *1080* está definido en la configuración de vagrant, este puerto se abre en el localhost y es re enviado al puerto *80* de la maquina cms.  

## Limpieza

Si realizaste cambios en la configuración de ansible y su inventario local y deseas revertir los cambios ejecuta make con el parámetro *clean*, por ejemplo:

```shell
root@devops:/vagrant# make clean
```

**IMPORTANTE:** Este comando debe ejecutarse en la máquina virtual devops.

## Destrucción

Para destruir el ambiente de desarrollo local que creaste, debes ejecutar el comando make con el parámetro *destroy*, por ejemplo:

```shell
$ make destroy
```

**IMPORTANTE:** Este comando debe ejecutarse en tu equipo local, en donde corre vagrant.

## Personalización

Cuando se construye la plataforma de orquestación, esta ya incluye algunas configuraciones parametrizadas para un ambiente de
desarrollo local, sin embargo, se puede usar como base para desplegar a otros ambientes, tales como pruebas o incluso puede
desplegar algunos roles a producción.

Los parámetros principales con los que se construye la plataforma son:

 * Proyecto: local
 * Ambiente: development
 * Dominio DNS: example.com
 * Servidor GIT: github.com

Algunos de estos parámetros están definidos en el archivo del inventario, es decir, en */etc/ansible/inventory/local/development*.

Esta ruta hacía el inventario se define en el archivo */etc/ansible/ansible.cfg*.

La llave SSH con la que ansible se conecta a los servidores para realizar las tareas de automatización es una llave generica, si va a implementar este sistema en un ambiente de pruebas o producción debe asegurarse de cambiar la llave, la configuración actual está así en el archivo de inventario:

```
...
...
...

[central-devops:vars]
ansible_ssh_port=22
ansible_ssh_user=root
ansible_ssh_private_key_file=/etc/ansible/.ssh/example.dev.infra.admin.rsa

...
...
...

[central-cms:vars]
ansible_ssh_port=22
ansible_ssh_user=root
ansible_ssh_private_key_file=/etc/ansible/.ssh/example.dev.infra.admin.rsa

```
Como se puede ver, las variables de conexión para los grupos de servidores central-devops y central-cms usan la llave definida por el parámetro *ansible_ssh_private_key_file*, en este caso la llave se almacena en el subdirectorio *.ssh* de ansible.

Es recomendado que generes tu propia llave SSH y la parametrices en el inventario.

## Mejores prácticas

Estos playbooks se deben alinear a la estructura actual de ansible que es proyecto:clase servidor:tipo servidor: centro servidor.

Para construir la estructura del rol se debe usar ansible-galaxy, el nombre debe ser rol-service.

**IMPORTANTE:** Los roles deben usar guión medio para separar las palabras que componen el nombre del rol, se aconseja que los nombres de los roles terminen con "-service".

Se deben seguir las mejores prácticas de edición en relación a :

 * Escribir nombres de tareas descriptivas y en inglés.
 * Identado basado en espacios en blanco.
 * Lineas en blanco para separar cada tarea
 * Uso de variables en formato ansible 2.x, ejemplo ansible_ssh_host en lugar de ansible_host.
 * Uso de variables especificas para el componente en vars/main.yml.
 * Uso de variables especificas para el ambiente en defaults/main.yml.
 * Uso de variables especificas para el centro en inventory/pmc/production.
 * Uso de variables estandar en base a el inventario o facts para evitar re escribir funciones de shell para capturar datos.
 * Se deben usar las funciones de python como split para sacar variables personalizadas siempre en base a variables exitentes en inventario, rol o fact.
 * Se deben usar tags para cada tarea, si son generales que se use la etiqueta general rol_service, note el guion bajo, no se usan guión medio "-".

Una vez desarrollado el playbook del rol, se debe agregar el rol al playbook de cada tipo de servidor abajo del rol de snmp-service.

Se deben documentar los tags en el documento de la matriz del sistema de gestión de configuraciones que se adjunta.

Los cambios deben integrarse al repositorio git ansible en la rama development y realizar un merge request para que se valide.

## Referencias

En esta sección hacemos referencia a documentación que usamos de ejemplo para construir este sistema y su integración con otras herramientas de Orquestación de TI.

Git:
https://git-scm.com

Git Flow:
https://danielkummer.github.io/git-flow-cheatsheet/

THE INSIDE PLAYBOOK - ORCHESTRATION, YOU KEEP USING THAT WORD:
http://www.ansible.com/blog/orchestration-you-keep-using-that-word

Jenkins - Building a software project
https://wiki.jenkins-ci.org/display/JENKINS/Building+a+software+project

ImmutableServer:
http://martinfowler.com/bliki/ImmutableServer.html

What do “immutable servers” and AWS environments have in common?:
https://elasticbox.com/blog/immutable-server-environments-in-aws/

Rethinking building on the cloud: Part 4: Immutable Servers:
https://www.thoughtworks.com/insights/blog/rethinking-building-cloud-part-4-immutable-servers

Jenkins - Ansible Plugin:
https://wiki.jenkins-ci.org/display/JENKINS/Ansible+Plugin

Automated Servers and Deployments with Ansible & Jenkins:
https://chromatichq.com/blog/automated-servers-and-deployments-ansible-jenkins

Understanding GitHub Flow:
https://guides.github.com/introduction/flow/

Continuous Integration, Delivery or Deployment with Jenkins, Docker and Ansible:
https://technologyconversations.com/2015/02/11/continuous-integration-delivery-or-deployment-with-jenkins-docker-and-ansible/

Vert.x featuring Continuous Delivery with Jenkins and Ansible:
http://vertx.io/blog/vert-x-featuring-continuous-delivery-with-jenkins-and-ansible/
