#!/bin/bash

#
# script: generate-ssh-keys.sh
# author: Jorge Armando Medina
# desc: Generate SSH RSA 4096 keys for ansible and jenkins.

# Enable debug mode and log to file
export DEBUG=1
LOG=/var/log/generate-ssh-keys.log
[ -n "$DEBUG" ] && exec < /dev/stdin > $LOG 2>&1

# Bash debug mode
[ -n "$DEBUG" ] && set -x

# Stop on errors
#set -e

# vars
ANSIBLE_USER=root
JENKINS_USER=jenkins
PROJECT_NAME=local
PROJECT_DOMAIN=example.com

# main

# Create ssh directory
pwd
mkdir .ssh
chmod 700 .ssh
cd .ssh

echo "Generating RSA/4096 SSH keys for ansible user ${ANSIBLE_USER}."
ssh-keygen -t rsa -b 4096 -C "ansible@${PROJECT_DOMAIN}" -f id-${PROJECT_NAME}-ansible.rsa -q -N ""

echo "Generating RSA/4096 SSH keys for jenkins user ${JENKINS_USER}."
ssh-keygen -t rsa -b 4096 -C "jenkins@${PROJECT_DOMAIN}" -f id-${PROJECT_NAME}-jenkins.rsa -q -N ""
