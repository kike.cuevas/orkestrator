bacula-service
==============

Tasks for setting up bacula server.

Requirements
------------

Ubuntu based system.

Role Variables
--------------


Dependencies
 -----------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - bacula-service

Tags
----

You should use --tags=bacula_service.

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@koolops.com.mx.
