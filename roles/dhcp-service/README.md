dhcp-service
===========

Tasks for setting up local dhcp service.

Requirements
------------

Ubuntu based system.

Role Variables
--------------


Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - dhcp-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@koolops.com.mx.
