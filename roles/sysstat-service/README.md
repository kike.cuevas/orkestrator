sysstat-service
===============

Tasks for setting up sysstat for sar monitoring.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

Not at the moment.


Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - sysstat-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@koolops.com.mx.
