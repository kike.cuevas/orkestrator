dns-service
===========

Tasks for setting up local dns service.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

Default dns servers:

dns_nameserver1: 208.67.222.222
dns_nameserver2: 208.67.220.220

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - dns-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@koolops.com.mx.
