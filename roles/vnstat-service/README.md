vnstat-service
==============

Common tasks for Linux Servers

Requirements
------------

Ubuntu based system.

Role Variables
--------------

#lan_interface: eth0
lan_interface: ens3


Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - vnstat-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@koolops.com.mx.
