nrpe-service
============

Tasks for setup nagios nrpe service o nodes.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

nagios_server: 127.0.0.1

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - nrpe-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@koolops.com.mx.
