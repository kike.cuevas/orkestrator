wordpress-service
=================

Tasks for setting up wordpress on LAMP.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

# These are for mysql server
mysql_root_user: root
mysql_root_password: s3cr3t!!!

# These are the WordPress database settings
wp_mysql_db: wordpress
wp_mysql_user: wp_dbuser
wp_mysql_password: s3cr3t!!!

# PHP Settings
php_date_timezone: America/Mexico_City
php_post_max_size: 128M
php_upload_max_filesize: 128M

# You shouldn't need to change this.
mysql_port: 3306

Dependencies
------------

Not at the moment

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - wordpress-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@koolops.com.mx.
